package pkg

import (
	"fmt"
	// "archive/zip"
	"encoding/json"
	// homedir "github.com/mitchellh/go-homedir"
	"bufio"
	// "errors"
	"bytes"
	"github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/exec"
	// "strings"
)

type Pkg struct {
	Id          string `json:"ID"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Directory   string
}

func (p *Pkg) Validate() bool {
	if p.Id == "" || p.Name == "" {
		return false
	}
	return true
}

func (p *Pkg) Write() bool {
	if p.Validate() {
		pathFile := p.Directory + "/.periuk"
		jsonString, err := json.Marshal(p)
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("Error while marshalling package file.")
		}
		err = ioutil.WriteFile(pathFile, []byte(jsonString), 0644)
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("Error while writing configuration file.")
		}
		return true
	}

	return false
}

func (p *Pkg) Init() {
	reader := bufio.NewScanner(os.Stdin)

	fmt.Print("Name (" + p.Name + "): ")
	_ = reader.Scan()
	if reader.Text() != "" {
		p.Name = reader.Text()
	}

	fmt.Print("Description: ")
	_ = reader.Scan()
	if reader.Text() != "" {
		p.Description = reader.Text()
	}

	p.Id = uuid.NewV4().String()
	return
}

func (p *Pkg) Read() {
	cwd, _ := os.Getwd()
	p.Directory = cwd
	pathFile := cwd + "/.periuk"

	file, err := os.OpenFile(pathFile, os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while opening configuration file.")
	}

	defer file.Close()

	content, err := ioutil.ReadFile(pathFile)

	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while reading config file")
		return
	}

	if string(content) != "" {
		if err := json.Unmarshal(content, p); err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("Error while converting config file")
		}
	}

}

func (p *Pkg) Zip() {
	log.Info("Starting to zip directory...")
	cmd := exec.Command("zip", "-X", "-r", p.Id+".zip", ".", "-x", "*.git*")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Error("Error while zipping directory")
	}
	log.Info("Done.")
}

func (p *Pkg) Save() {

}
