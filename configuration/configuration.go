package configuration

import (
	"encoding/json"
	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
)

type Configuration struct {
	Email        string `json:"email"`
	AccessKey    string `json:"access_key"`
	AccessSecret string `json:"access_secret"`
}

func (c *Configuration) Validate() bool {
	if c.Email == "" || c.AccessKey == "" || c.AccessSecret == "" {
		return false
	}
	return true
}

func (c *Configuration) Read() {
	homeDir, err := homedir.Dir()
	if err != nil {
		log.Error("Error while opening home directory.")
		return
	}

	pathDir := homeDir + "/.periuk/"
	pathFile := pathDir + "config"

	_, err = os.Stat(pathDir)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while opening home directory.")
		return
	}

	_, err = os.OpenFile(pathFile, os.O_RDONLY|os.O_CREATE, 0666)

	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while opening configuration file.")
	}

	content, err := ioutil.ReadFile(pathFile)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while reading config file")
		return
	}

	if err := json.Unmarshal(content, c); err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while converting config file")
	}
}

func (c *Configuration) Write() {
	homeDir, err := homedir.Dir()
	if err != nil {
		log.Error("Error while opening home directory.")
		return
	}

	pathDir := homeDir + "/.periuk/"
	pathFile := pathDir + "config"

	_, err = os.Stat(pathDir)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while opening home directory.")
		return
	}

	_, err = os.OpenFile(pathFile, os.O_RDONLY|os.O_CREATE, 0666)

	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while opening configuration file.")
	}

	jsonString, err := json.Marshal(c)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while marshalling configuration file.")
	}
	err = ioutil.WriteFile(pathFile, []byte(jsonString), 0644)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Error while writing configuration file.")
	}
}
