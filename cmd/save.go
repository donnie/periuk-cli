// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bitbucket.org/donnie/periuk-cli/pkg"
	// "fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	// "os"
	// "path/filepath"
)

// saveCmd represents the save command
var saveCmd = &cobra.Command{
	Use:   "save",
	Short: "Scan current directory and upload as a package",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		save()
	},
}

// var pkgs []pkg.Pkg

// func walkFn(path string, info os.FileInfo, err error) error {
// 	if info.Name() == ".periuk" {
// 		var pkg pkg.Pkg
// 		pkg.Directory, _ = filepath.Abs(filepath.Dir(path))
// 		pkgs = append(pkgs, pkg)
// 	}
// 	return nil
// }

func save() {
	// log.Info("Scanning current directory")
	// pkgs = make([]pkg.Pkg, 0)
	// filepath.Walk("./", walkFn)
	// log.Printf("Found (%d) package file.", len(pkgs))
	log.Info("Checking package file")
	var pkg pkg.Pkg

	// Check package file in cwd
	// If found then get the value
	pkg.Read()
	// If not found then create the file and ask user to fill in the details
	if pkg.Validate() == false {
		log.Info("I didn't find any package file.")
		pkg.Init()
	}

	if !pkg.Write() {
		return
	}

	// Create zip file
	pkg.Zip()
	// Save data

	// Upload file

}

func init() {
	packageCmd.AddCommand(saveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// saveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// saveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
