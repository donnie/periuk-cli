// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bufio"
	"fmt"
	// "github.com/fatih/color"
	"bitbucket.org/donnie/periuk-cli/configuration"
	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
)

// configureCmd represents the configure command
var configureCmd = &cobra.Command{
	Use:   "configure",
	Short: "Initialisation of periuk credentials",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 {
			log.Println("configure does not accept any parameters. Sorry, but I have to ignore it.")
		}
		configure()

	},
}

func configure() {
	log.Println("Looking for configuration...")
	home, err := homedir.Dir()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	rootDir, err := os.Stat(home + "/.periuk")
	if err != nil {
		log.Println(err)

	}
	if rootDir == nil {
		log.Println("Creating configuration directory")
		err = os.Mkdir(home+"/.periuk/", 0755)

		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
	}
	var c configuration.Configuration
	reader := bufio.NewScanner(os.Stdin)
	// color.Blue("Access Key:", "text")
	c.Read()

	fmt.Print("Email (" + c.Email + "): ")
	_ = reader.Scan()
	if reader.Text() != "" {
		c.Email = reader.Text()
	}

	fmt.Print("Access Key (" + c.AccessKey + "): ")
	_ = reader.Scan()
	if reader.Text() != "" {
		c.AccessKey = reader.Text()
	}
	fmt.Print("Access Secret (" + c.AccessSecret + "): ")
	_ = reader.Scan()
	if reader.Text() != "" {
		c.AccessSecret = reader.Text()
	}

	c.Write()
	log.Info("Writing to file")
}

func init() {
	RootCmd.AddCommand(configureCmd)

}
